---
title: 'Homepage'
description: "Home page for the School."
intro_image: "images/illustrations/school-home-picture.jpg"
intro_image_absolute: false
intro_image_hide_on_mobile: true
---
